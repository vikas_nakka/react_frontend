import React, { Component } from 'react';
import { Button, Glyphicon } from 'react-bootstrap';

//new video
import store from '../store';

const styles = {
  products: {
    display: 'flex',
    alignItems: 'stretch',
    flexWrap: 'wrap'
  },
  product: {
    width: '220px',
    marginLeft: 10,
    marginRight: 10
  }
};

class ProductList extends Component {
  constructor() {
    super();
    this.addToCart = this.addToCart.bind(this);

    this.state = {
      products: [
        { id: 1, name: "Tommy HilFigure", price: 299, image: "https://rukminim1.flixcart.com/image/880/1056/jzog9e80/shirt/h/h/q/m-cb-chaina-black-tap-in-original-imafjn5rg8kh68hx.jpeg?q=50" },
        { id: 2, name: "US polo", price: 99, image: "https://rukminim1.flixcart.com/image/880/1056/kfoapow0-0/shirt/n/q/3/s-hlsh010257-highlander-original-imafw2gpypcqzcqt.jpeg?q=50" },
        { id: 3, name: "Nike", price: 149, image: "https://rukminim1.flixcart.com/image/880/1056/kjg1jm80-0/shirt/e/r/t/l-cc201-dustypink-dennis-lingo-original-imafzygg2fmn7erc.jpeg?q=50" },
        { id: 4, name: "UCB", price: 149, image: "https://rukminim1.flixcart.com/image/880/1056/kfoapow0-0/shirt/q/h/w/l-hlsh009599-highlander-original-imafw2gpphzqbmxj.jpeg?q=50" },
        { id: 5, name: "Wrong", price: 149, image: "https://rukminim1.flixcart.com/image/880/1056/ki4w0i80-0/jacket/j/l/z/3xl-trdblhdfuljacket-k18-tripr-original-imafxzzxbktjq9kp.jpeg?q=50" },
        { id: 6, name: "John Miller", price: 149, image: "https://rukminim1.flixcart.com/image/880/1056/kfoapow0-0/shirt/c/2/d/l-cm-st153-cobio-man-original-imafw2gz26hskggm.jpeg?q=50" },
      
      ]
    }
  }

  render() {
    return (
      <div style={styles.products}>
        {this.state.products.map(product =>
          <div className="thumbnail" style={styles.product} key={product.id}>
            <img src={product.image} alt={product.name} />
            <div className="caption">
              <h4>{product.name}</h4>
              <p>
                <Button bsStyle="primary" onClick={() => this.addToCart(product)} role="button" disabled={product.inventory <= 0}>${product.price} <Glyphicon glyph="shopping-cart" /></Button>
              </p>
            </div>
          </div>
        )}
      </div>
    );
  }

  //more code
  addToCart(product) {
    store.dispatch({
      type: "ADD_TO_CART",
      product
    });
  }
}

export default ProductList;
